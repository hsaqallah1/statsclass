import numpy as np
import scipy.stats as sct

cl = .95

average_defects = .18

sample_size = 150

no_of_defects = 23

#Average of sample is 23/150 = 15.33% (to the left of average defects)
#H0: 18% of spoons or higher are defects. p >= 18%
#H1: Less than 18% of spoons are defects.

p = average_defects
q = 1 - p

mu_sample = p
sigma = np.sqrt(p*q)

sigma_sample = sigma/np.sqrt (sample_size)

p_sample = no_of_defects/sample_size

z = (p_sample - mu_sample)/sigma_sample

print ('z = ' + str(z))

print('Probability of sample occurring is ' + str(sct.norm.cdf(z)))

