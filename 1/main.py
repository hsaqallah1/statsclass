import numpy as np
from matplotlib import pyplot as plt
import scipy.stats as sct

data = [
    {'Gender': 'Men', 'mean': 175.5, 'stddev': 7.4},
    {'Gender': 'Women', 'mean': 161.8, 'stddev': 6.9}
]

pop_size = 1000

for d in data:
    W = np.random.normal(d['mean'], d['stddev'], pop_size)

    #Draw histograms
    plt.figure()
    bins = np.arange(50, 250, 1)
    plt.xlim([min(W)-5, max(W)+5])
    plt.hist(W, bins=bins)
    plt.title('Historgram for ' + d['Gender'] + '\'s heights')
    plt.xlabel('height in cm')
    plt.ylabel('\#')

    #Calculate exact height
    print('Using Inverse Survival Function, minimum height for ' + d['Gender'] + ' = ' + str(sct.norm.isf(q=.022, loc = d['mean'], scale = d['stddev']))+" cm")

    #Calculate using primitive method
    #First sort
    W_sorted = np.sort(W) #This is ascending

    #Get the element at 100 - 2.2 = 97.8%
    print('Using primitive method, minimum height for ' + d['Gender'] + ' = ' + str(W_sorted[int(.978 * pop_size)]) + " cm")

plt.show()


