import numpy as np
import scipy.stats as sct

def getZ(x_crit, mu_of_samples, sigma_of_samples):
    return ((x_crit-mu_of_samples)/sigma_of_samples)


mu = 95.70
sigma = 1247.00

no_of_trades = 100
mu_samples = mu
sigma_samples = sigma / np.sqrt(no_of_trades)

#1 probability of making loss in a week
profit = 0.00
profit_per_trade = profit / no_of_trades

z = getZ(profit_per_trade, mu_samples, sigma_samples)


print('z = ' + str(z))
print('Probability of making loss, i.e. profit = 0 or less is ' + str(sct.norm.cdf(z)))

#2 probability of making more than $20000 in a week
profit = 20000.00
profit_per_trade = profit / no_of_trades
z = getZ(profit_per_trade, mu_samples, sigma_samples)
print('z = ' + str(z))

print('Probability of making more than $20k is ' + str(1-sct.norm.cdf(z)))









