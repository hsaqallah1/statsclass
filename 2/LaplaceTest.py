import numpy as np
from matplotlib import pyplot as plt
import scipy.stats as sct


mu = 95.7
sigma = 1247.00

trades_per_week = 100

no_of_weeks = 100000

weekly_profits_per_trade = []

for i in range(0, no_of_weeks):
    weekly_profits_per_trade.append(np.sum(np.random.laplace(loc=mu, scale=sigma, size=trades_per_week))/trades_per_week)

profit_per_trade = 20000/trades_per_week
greater_than_20k = sum(x >= profit_per_trade for x in weekly_profits_per_trade) / len(weekly_profits_per_trade)

print(greater_than_20k)