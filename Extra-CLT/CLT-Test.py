import numpy as np


population_size = 100000

population = np.random.laplace(3,2,population_size)

sample_size = 100

no_of_samples = 50000

distribution_sample_averages = []

for i in range(0, no_of_samples):
    distribution_sample_averages.append(np.average(np.random.choice(population, size=sample_size)))

print('Mean of population = ' + str(np.average(population)))
print('Mean of distribution samples = ' + str(np.average(distribution_sample_averages)))

pop_stddev = np.std(population)
print('stddev of population = ' + str(pop_stddev))
print('stddev of distribution samples = ' + str(np.std(distribution_sample_averages)))
print('Predicted stddev of distribution samples = ' + str(pop_stddev/np.sqrt(sample_size)))